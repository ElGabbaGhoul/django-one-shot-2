from django.contrib import admin
from .models import TodoList, TodoItem


class TodoListAdmin(TodoList):
    pass


class TodoItemAdmin(TodoItem):
    pass


admin.site.register(TodoList)
admin.site.register(TodoItem)
