from django import forms
from .models import TodoList, TodoItem


class TodoListForm(forms.ModelForm):
    class Meta:
        model = TodoList
        exclude = ["created_on"]


class TodoItemForm(forms.ModelForm):
    class Meta:
        model = TodoItem
        exclude = []
