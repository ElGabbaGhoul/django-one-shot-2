from django.urls import path
from .views import (
    TodoListView,
    TodoListDetail,
    CreateList,
    UpdateList,
    DeleteList,
    CreateItem,
    UpdateItem,
)

urlpatterns = [
    path("", TodoListView.as_view(), name="todo_list_list"),
    path("<int:pk>/", TodoListDetail.as_view(), name="todo_list_detail"),
    path("create/", CreateList.as_view(), name="todo_list_create"),
    path("<int:pk>/edit/", UpdateList.as_view(), name="todo_list_update"),
    path("<int:pk>/delete/", DeleteList.as_view(), name="todo_list_delete"),
    path("items/create/", CreateItem.as_view(), name="todo_item_create"),
    path("items/<int:pk>/edit/", UpdateItem.as_view(), name="todo_item_update"),
]
