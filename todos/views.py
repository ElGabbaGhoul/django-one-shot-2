from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from todos.forms import TodoListForm
from django.views.generic import CreateView, UpdateView, DetailView, DeleteView
from django.views.generic.list import ListView


from todos.models import TodoList, TodoItem


class TodoListView(ListView):
    model = TodoList
    template_name = "lists/list.html"
    context_object_name = "todolistview"

    def get_success_url(self):
        return reverse("todo_list_list", kwargs={"pk": self.object.list.pk})


class TodoListDetail(DetailView):
    model = TodoList
    template_name = "lists/detail.html"
    context_object_name = "todolistdetail"

    def get_success_url(self):
        return reverse("todo_list_detail", kwargs={"pk": self.object.list.pk})


class CreateList(CreateView):
    model = TodoList
    fields = ["name"]
    template_name = "lists/create.html"

    def get_success_url(self):
        return reverse("todo_list_detail", kwargs={"pk": self.object.pk})


class UpdateList(UpdateView):
    model = TodoList
    fields = ["name"]
    template_name = "lists/update.html"

    def get_success_url(self):
        return reverse("todo_list_detail", kwargs={"pk": self.object.pk})


class DeleteList(DeleteView):
    model = TodoList
    fields = ["name"]
    template_name = "lists/delete.html"

    def get_success_url(self):
        return reverse("todo_list_list")


class CreateItem(CreateView):
    model = TodoItem
    fields = ["task", "due_date", "is_completed", "list"]
    template_name = "items/create.html"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.pk])


class UpdateItem(UpdateView):
    model = TodoItem
    fields = ["task", "due_date", "is_completed", "list"]
    template_name = "items/update.html"

    def get_success_url(self):
        return reverse("todo_list_detail", kwargs={"pk": self.object.pk})
